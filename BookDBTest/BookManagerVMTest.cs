﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BookDB.Books;
using NUnit.Framework;

namespace BookDBTest
{
    [TestFixture]
    public class BookManagerVMTest
    {
        private Book book;

        [SetUp]
        public void Init()
        {
            GC.Collect();
        }

        [Test, TestCaseSource(typeof(BookManagerVMTestData), "BookValidISBNTestData")]
        public void BookValidISBNTest(Tuple<string, string> testData)
        {
            book = new Book
            {
                ISBN13 = testData.Item1
            };

            Assert.AreEqual(book.ISBN13, testData.Item2);
        }

        [Test, TestCaseSource(typeof(BookManagerVMTestData), "BookInvalidISBNTestData")]
        public void BookInvalidISBNTest(string testData)
        {
            book = new Book
            {
                ISBN13 = testData
            };

            Assert.AreEqual(book.ISBN13, string.Empty);
        }
    }

    public static class BookManagerVMTestData
    {
        public static IEnumerable BookValidISBNTestData
        {
            get
            {
                yield return new Tuple<string,string>("","");
                yield return new Tuple<string, string>("9784567891234", "9784567891234");
                yield return new Tuple<string, string>("9780000000000", "9780000000000");
                yield return new Tuple<string, string>("9783334354354", "9783334354354");
                yield return new Tuple<string, string>("9790000000000", "9790000000000");
                yield return new Tuple<string, string>("978-000-00-000-00", "9780000000000");
                yield return new Tuple<string, string>("978-000 00 000-00", "9780000000000");
                yield return new Tuple<string, string>("978-------00000000-00", "9780000000000");
                yield return new Tuple<string, string>("978 333 43 54354", "9783334354354");
                yield return new Tuple<string, string>("   979 0 0 0     00 00 0 0----0----", "9790000000000");
                yield return new Tuple<string, string>("           ", "");
            }
        }
        public static IEnumerable BookInvalidISBNTestData
        {
            get
            {
                yield return null;
                yield return "   ---   -     ";
                yield return "-     ";
                yield return "asd";
                yield return "ISBN: 9783334354354";
                yield return "97833343543549";
                yield return "9773334354354";
                yield return "9703334354354";
                yield return "978333435454";
                yield return "b9783334354354";
                yield return "b978333435435";
                yield return "b9783334xxx35";
                yield return "-------------";
                yield return "978:1234556:123";
            }
        }
    }
}
