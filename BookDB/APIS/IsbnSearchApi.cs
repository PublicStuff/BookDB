﻿using Google.Apis.Books.v1;
using Google.Apis.Books.v1.Data;
using Google.Apis.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;

namespace BookDB.APIS
{
    internal static class IsbnSearchApi
    {
        private static BooksService service; 

        static IsbnSearchApi()
        {
            service = new BooksService(
                new BaseClientService.Initializer
                {
                    ApplicationName = "BookCha",
                    ApiKey = "AIzaSyCmacy9B-_X9ljeCvieqgcPZ4CVoo6k5Kg"
                });
        }

        public static async Task<Volume> GetVolumeAsync(string isbn)
        {
            var result = await service.Volumes.List(isbn).ExecuteAsync();

            if (result != null && result.Items != null)
            {
                return result.Items.FirstOrDefault();
            }

            return null;
        }
    }
}
