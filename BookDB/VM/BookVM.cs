﻿using BookDB.Books;
using BookDB.Commands;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace BookDB.VM
{
    public class BookVM : INotifyPropertyChanged
    {
        private readonly Book book;

        //public BookVM():this(new Book(), null)
        //{
        //    // todo: delete this and disable can add in listbox
        //}

        public BookVM(Book book, ICommand deleteCommand)
        {
            this.book = book ?? throw new ArgumentNullException(nameof(this.book));
            this.RemoveCommand = deleteCommand;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public ICommand RemoveCommand
        {
            get;
        }

        public string Author
        {
            get
            {
                return this.book.Author;
            }

            set
            {
                this.book.Author = value;
                this.Notify(nameof(this.Author));
            }
        }

        public Genre Genre
        {
            get
            {
                return this.book.Genre;
            }

            set
            {
                this.book.Genre = value;
                this.Notify(nameof(this.Genre));
            }
        }

        public string ISBN13
        {
            get
            {
                return this.book.ISBN13;
            }

            set
            {
                this.book.ISBN13 = value;
                this.Notify(nameof(this.ISBN13));
            }
        }

        public Score Score
        {
            get
            {
                return this.book.Score;
            }

            set
            {
                this.book.Score = value;
                this.Notify(nameof(this.Score));
            }
        }

        public string Title
        {
            get
            {
                return this.book.Title;
            }

            set
            {
                this.book.Title = value;
                this.Notify(nameof(this.Title));
            }
        }

        protected virtual void Notify([CallerMemberName]string propName = null)
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propName));
        }

        internal Book GetBook()
        {
            return this.book;
        }
    }
}
