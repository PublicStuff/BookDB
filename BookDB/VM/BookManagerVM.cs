﻿using BookDB.Books;
using BookDB.Commands;
using BookDB.Files;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Controls;
using System.Windows;
using Microsoft.Win32;
using BookDB.APIS;
using System.Collections.Concurrent;
using Google.Apis.Books.v1.Data;

namespace BookDB.VM
{
    public class BookManagerVM : INotifyPropertyChanged
    {
        private ObservableCollection<BookVM> booksList;
        private ICommand removeCommand;
        //private IList<BookVM> selectedBooks;
        private Queue<BookVM> deleteQueue;

        public BookManagerVM()
        {
            this.removeCommand = new Command(obj =>
            {
                if (obj is BookVM bookVM)
                {
                    this.BookList.Remove(bookVM);
                }
            });
            
            this.BookList = new ObservableCollection<BookVM>(this.ConvertBooks(FileIO.ReadData()));

            this.deleteQueue = new Queue<BookVM>();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public ICommand AddBookCommand
        {
            get
            {
                return new Command(obj =>
                {
                    var newBook = new BookVM(new Book(), this.removeCommand);
                    this.BookList.Add(newBook);
                });
            }
        }

        public ICommand GetIsbnInfoCommand
        {
            get
            {
                return new Command(async obj =>
                {
                    var boxRes = MessageBox.Show(
    "Runs a Google Books search to get data for this Isbn.\nThis will overwrite your current author and title data!\n\n",
    "Attention!",
    MessageBoxButton.OKCancel,
    MessageBoxImage.Warning);

                    if (boxRes != MessageBoxResult.OK)
                    {
                        return;
                    }

                    if (obj is System.Collections.IList convList && convList.Count > 0 && convList[0] is BookVM book)
                    {
                        Volume result = null;

                        try
                        {
                            result = await IsbnSearchApi.GetVolumeAsync(book.ISBN13);
                        }
                        catch
                        {
                            MessageBox.Show("Error while executing the requested operation!", "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
                            return;
                        }

                        if (result != null && result.VolumeInfo != null)
                        {
                            book.Author = result.VolumeInfo.Authors.Aggregate((a,b) => a + "; " + b);
                            book.Title = result.VolumeInfo.Title;
                            //book.Title = $"{result.VolumeInfo.Title}  [{result.VolumeInfo.SeriesInfo.ShortSeriesBookTitle}]";
                            MessageBox.Show("Successfully read author info", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                            return;
                        }
                    }

                    MessageBox.Show("Error while executing the requested operation!", "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
                });
            }
        }

        public ICommand RemoveSelectedBooksCommand
        {
            get
            {
                return new Command(obj =>
                {
                    if (obj is System.Collections.IList convList)
                    {
                        this.deleteQueue.Clear();

                        foreach (var something in convList)
                        {
                            if (something is BookVM book)
                            {
                                deleteQueue.Enqueue((BookVM)book);
                            }
                        }

                        while (deleteQueue.Count > 0)
                        {
                            this.BookList.Remove(deleteQueue.Dequeue());
                        }
                    }
                });
            }
        }

        public ICommand ShowInfoCommand
        {
            get
            {
                return new Command(obj =>
                {
                    MessageBox.Show(
                        "BookCha \nVersion:     0.2 Alpha \nAuthor:      Patrik Plöchl \nE-Mail:       info.bookcha@gmail.com \nCopyright: ©2018 Patrik Plöchl",
                        "Info",
                        MessageBoxButton.OK,
                        MessageBoxImage.Information
                        );
                    
                });
            }
        }

        public ICommand CloseWindowCommand
        {
            get
            {
                return new Command(obj =>
                {
                    if (obj is Window window)
                    {
                        this.CloseWindow(window);
                    }

                });
            }
        }

        public ICommand ExportCommand
        {
            get
            {
                return new Command(obj => 
                {
                    SaveFileDialog saveDialog = new SaveFileDialog()
                    {
                        Filter = "XML file (*.xml)|*.xml",
                        //CheckFileExists = true,
                        CheckPathExists = true
                        //InitialDirectory = FileIO.DefaultPath                        
                    };

                    if (saveDialog.ShowDialog() == true)
                    {
                        if (FileIO.SaveData(this.BookList.Select(vm => vm.GetBook()).ToList<Book>(), saveDialog.FileName))
                        {
                            MessageBox.Show("List successfully exported.", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                        }
                        else
                        {
                            MessageBox.Show("Couldn't export list.", "Oh snap!", MessageBoxButton.OK, MessageBoxImage.Error);
                        }
                    }
                });
            }
        }

        public ICommand ImportCommand
        {
            get
            {
                return new Command(obj =>
                {
                    var result = MessageBox.Show(
                        "If you import a list, it will overwrite your current list!\nIf you want to save your current list press cancel and export it so you can import it later.\n\n",
                        "Attention!",
                        MessageBoxButton.OKCancel,
                        MessageBoxImage.Warning);

                    if (result != MessageBoxResult.OK)
                    {
                        return;
                    }


                    OpenFileDialog saveDialog = new OpenFileDialog()
                    {
                        Filter = "XML file (*.xml)|*.xml",
                        Multiselect = false,
                        AddExtension = true,
                        CheckFileExists = true,
                        CheckPathExists = true
                        //InitialDirectory = FileIO.DefaultPath                        
                    };

                    if (saveDialog.ShowDialog() == true)
                    {
                        var newList = FileIO.ReadData(saveDialog.FileName, out bool success);

                        if (success)
                        {
                            this.BookList = new ObservableCollection<BookVM>(this.ConvertBooks(newList));
                            MessageBox.Show("List successfully imported.", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                        }
                        else
                        {
                            MessageBox.Show("Couldn't import list.", "Oh snap!", MessageBoxButton.OK, MessageBoxImage.Error);
                        }
                    }
                });
            }
        }

        //public IList<BookVM> SelectedBooks
        //{
        //    get
        //    {
        //        return this.selectedBooks;
        //    }

        //    set
        //    {
        //        this.selectedBooks = value;
        //        this.Notify(nameof(this.SelectedBooks));
        //    }
        //}

        public ObservableCollection<BookVM> BookList
        {
            get
            {
                return this.booksList;
            }

            private set
            {
                this.booksList = value;
                this.Notify(nameof(this.BookList));
            }
        }

        internal void SaveData()
        {
            FileIO.SaveData(this.BookList.Select(vm => vm.GetBook()).ToList<Book>());
        }

        internal void CloseWindow(Window window)
        {
            window.Close();
        }

        protected virtual void Notify([CallerMemberName]string propName = null)
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propName));
        }

        private ObservableCollection<BookVM> ConvertBooks(List<Book> bookList)
        {
            if (bookList == null)
            {
                return null;
            }

            return new ObservableCollection<BookVM>(bookList.Select<Book, BookVM>(book => new BookVM(book, this.removeCommand)));
        }
    }
}
