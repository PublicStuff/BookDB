﻿using BookDB.Books;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Soap;
using System.Xml.Serialization;

namespace BookDB.Files
{
    public static class FileIO
    {
        private static readonly string fullFileName;
        private static readonly string path;
        private static XmlSerializer formatter;

        static FileIO()
        {
            formatter = new XmlSerializer(typeof(List<Book>));

            path = Directory.GetCurrentDirectory() + @"\db\";
            
            if (!Directory.Exists(path))
            {
                try
                {
                    Directory.CreateDirectory(path);
                }
                catch
                {
                }
            }

            fullFileName = path + "books.xml";
        }

        public static string DefaultPath
        {
            get
            {
                return path;
            }
        }

        public static bool SaveData(List<Book> books, string path)
        {
            try
            {
                using (FileStream fileStream = new FileStream(path, FileMode.Create, FileAccess.Write, FileShare.None))
                {
                    if (books != null)
                    {
                        formatter.Serialize(fileStream, books);
                    }
                }
            }
            catch
            {
                return false;
            }

            return true;
        }

        public static bool SaveData(List<Book> books)
        {
            return SaveData(books, fullFileName);
        }

        public static List<Book> ReadData(string path, out bool success)
        {
            success = true;

            List<Book> results = new List<Book>();

            try
            {
                using (FileStream fileStream = new FileStream(path, FileMode.OpenOrCreate, FileAccess.Read, FileShare.Read))
                {
                    results = (List<Book>)formatter.Deserialize(fileStream);
                }
            }
            catch
            {
                success = false;
            }

            return results;
        }

        public static List<Book> ReadData()
        {
            return ReadData(fullFileName, out _);
        }
    }
}