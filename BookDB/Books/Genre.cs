﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookDB.Books
{
    public enum Genre
    {
        Other = 0,
        Action,
        Adventure,
        Anthology,
        Art,
        Autobiographies,
        Biographies,
        Childrens,
        Comics,
        Cookbooks,
        Diaries,
        Dictionaries,
        Drama,
        Encyclopedias,
        Fantasy,
        Guide,
        Health,
        History,
        Horror,
        Journals,
        Math,
        Mystery,
        Poetry,
        PrayerBooks,
        Psychology,
        Religion,
        Romance,
        Satire,
        Science,
        ScienceFiction,
        SelfHelp,
        Travel
    }
}
