﻿using BookDB.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BookDB.Books
{
    [Serializable()]
    public class Book
    {
        private string isbn13;

        public Book()
        {
            this.ISBN13 = string.Empty;
            this.Author = string.Empty;
            this.Title = string.Empty;
        }

        public string Author
        {
            get;
            set;
        }

        public Score Score
        {
            get;
            set;
        }

        public Genre Genre
        {
            get;
            set;
        }

        public string ISBN13
        {
            get
            {
                return this.isbn13;
            }

            set
            {
                if (value == null)
                {
                    return;
                }

                var str = value.Replace(" ", string.Empty);

                if (str == string.Empty)
                {
                    this.isbn13 = str;
                    return;
                }

                str = str.Replace("-", "");

                if (str.IsValidIsbn13Format())
                {
                    this.isbn13 = str;
                }
            }
        }

        public string Title
        {
            get;
            set;
        }
    }
}