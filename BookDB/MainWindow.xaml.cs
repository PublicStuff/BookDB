﻿using BookDB.VM;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BookDB
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private BookManagerVM managerVM;

        public MainWindow()
        {
            InitializeComponent();
            this.managerVM = (Application.Current as App).ManagerVM;
            this.DataContext = this.managerVM;
            this.Closed += MainWindow_Closed;

            CommandManager.RegisterClassInputBinding(typeof(DataGrid), 
                new InputBinding(DataGrid.BeginEditCommand, new KeyGesture(Key.Enter)));
            CommandManager.RegisterClassInputBinding(typeof(DataGrid),
                new InputBinding(DataGrid.DeleteCommand, new KeyGesture(Key.X,ModifierKeys.Control)));
        }

        private void MainWindow_Closed(object sender, EventArgs e)
        {
            if ((Application.Current as App).ManagerVM is BookManagerVM managerVM)
            {
                managerVM.SaveData();
            }
        }
    }
}
