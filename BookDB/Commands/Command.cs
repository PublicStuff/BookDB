﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace BookDB.Commands
{
    public class Command : ICommand
    {
        private Action<object> action;

        public Command(Action<object> action)
        {
            this.action = action;
        }

        public event EventHandler CanExecuteChanged;

        public bool CanExecute(object parameter)
        {
            // todo: implement canExecute

            return true;
        }

        public void Execute(object parameter)
        {
            this.action(parameter);
        }
    }
}
