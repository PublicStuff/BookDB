﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace BookDB.Utils
{
    public static class Extensions
    {
        private static Regex regex;

        static Extensions()
        {
            regex = new Regex(@"^(97(8|9))?\d{9}(\d|X)$", RegexOptions.IgnoreCase);
        }

        public static bool IsBetween<T>(this T item, T from, T to) where T : IComparable, IComparable<T>
        {
            return Comparer<T>.Default.Compare(item, from) >= 0 && Comparer<T>.Default.Compare(item, to) <= 0;
        }

        /// <summary>
        /// Checks whether the string seems to be a valid ISBN13 string. It only checks the format, but it doesn't check the checksum.
        /// </summary>
        /// <param name="item">The string to be checked.</param>
        /// <returns>True if the string seems to be valid.</returns>
        public static bool IsValidIsbn13Format(this string item)
        {
            if (item.Contains('-'))
            {
                item = new string(item.Where<char>(c => Char.IsDigit(c)).ToArray<char>());
            }

            return regex.IsMatch(item);

            //if (string.IsNullOrWhiteSpace(item))
            //{
            //    return false;
            //}


            //if (item.Length != 13)
            //{
            //    return false;
            //}

            //if (item.StartsWith("978") || item.StartsWith("979"))
            //{
            //    return true;
            //}
            //else
            //{
            //    return false;
            //}
        }
    }
}
