﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media;

namespace BookDB.Utils
{
    internal class ValidIsbnColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string isbn = value as string;

            if (isbn == null)
            {
                return Brushes.Red;
            }

            if (string.IsNullOrWhiteSpace(isbn))
            {
                return Brushes.LightGray;
            }

            if (isbn.IsValidIsbn13Format())
            {
                return Brushes.White;
            }

            return Brushes.Red;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
